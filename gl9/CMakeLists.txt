set(SRC_FILES
        ${PROJECT_SOURCE_DIR}/common/Application.cpp
        ${PROJECT_SOURCE_DIR}/common/DebugOutput.cpp
        ${PROJECT_SOURCE_DIR}/common/Camera.cpp
        ${PROJECT_SOURCE_DIR}/common/Mesh.cpp
        ${PROJECT_SOURCE_DIR}/common/ShaderProgram.cpp
        ${PROJECT_SOURCE_DIR}/common/Texture.cpp
        ${PROJECT_SOURCE_DIR}/common/QueryObject.cpp
        ${PROJECT_SOURCE_DIR}/common/ConditionalRender.cpp
        )

set(HEADER_FILES
        ${PROJECT_SOURCE_DIR}/common/Application.hpp
        ${PROJECT_SOURCE_DIR}/common/DebugOutput.h
        ${PROJECT_SOURCE_DIR}/common/Camera.hpp
        ${PROJECT_SOURCE_DIR}/common/Mesh.hpp
        ${PROJECT_SOURCE_DIR}/common/ShaderProgram.hpp
        ${PROJECT_SOURCE_DIR}/common/Texture.hpp
        ${PROJECT_SOURCE_DIR}/common/QueryObject.h
        ${PROJECT_SOURCE_DIR}/common/ConditionalRender.h
        )

MAKE_SAMPLE(gl9_Instancing)
MAKE_SAMPLE(gl9_GS_layered)
MAKE_SAMPLE(gl9_ConditionalRender)

COPY_RESOURCE(gl9shaders)

add_dependencies(gl9_Instancing gl9shaders)
add_dependencies(gl9_GS_layered gl9shaders)
add_dependencies(gl9_ConditionalRender gl9shaders)

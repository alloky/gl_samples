#include <Application.hpp>

#include <iostream>
#include <vector>
#include <numeric>

#include <Common.h>
#include <Texture.hpp>
#include <ShaderProgram.hpp>
#include <QueryObject.h>
#include <Mesh.hpp>

#define _USE_MATH_DEFINES
#include <math.h>


float G(float x, float sigma_inv) {
    float x_rel = x * sigma_inv;
    float g =expf(-0.5f * x_rel * x_rel) * sqrtf(0.5f / (float)M_PI) * sigma_inv;
    return g;
}

float G(int x, int width) {
    float g = G((float)x, 1.0f / width);
    return g;
}

void prepare_gauss_coeffs(std::vector<float>& gauss_coeffs, int sampling_width) {
    gauss_coeffs.clear();
    gauss_coeffs.resize((size_t)sampling_width + 1);
    for (int x = 0; x <= sampling_width; x++) {
        gauss_coeffs[x] = G(x, sampling_width);
    }
    // Normalize.
    float sum = gauss_coeffs[0];
    for (int x = 1; x <= sampling_width; x++) {
        sum += 2 * gauss_coeffs[x];
    }
    for (int x = 0; x <= sampling_width; x++) {
        gauss_coeffs[x] /= sum;
    }
}

/**
3 грани куба (вариант без индексов)
*/
class SampleApplication : public Application
{
public:
    //Идентификатор шейдерной программы
    ShaderProgramPtr blurProgram;

    TexturePtr inputTexture;
    TexturePtr outputTexture, intermTexture;

    void run() override {
        // Load shader:
        blurProgram = std::make_shared<ShaderProgram>();
        blurProgram->createProgramCompute("gpgpushaders/gauss_blur_trivial.comp");

        // Load input texture:
        inputTexture = loadTexture("images/image_4k.png");

        int width, height;
        inputTexture->getSize(width, height);

        // Create output texture of the same size:
        outputTexture = std::make_shared<Texture>();
        outputTexture->initStorage2D(1, GL_RGBA8, width, height);
        
        // Need two textures, one of them is intermediate
        intermTexture = std::make_shared<Texture>();
        intermTexture->initStorage2D(1, GL_RGBA8, width, height);

        glm::ivec2 gridSize(width, height);

        int kernelHalfSize = 32;
        std::vector<float> gauss_coeffs;
        prepare_gauss_coeffs(gauss_coeffs, kernelHalfSize);

        // Upload gauss coeffs to the uniform.
        int coeffsUniformLocation = glGetProgramResourceLocation(blurProgram->id(), GL_UNIFORM, "gauss_coeffs[0]");
        std::cout << "coeffs uniform location: " << coeffsUniformLocation << std::endl;
        glProgramUniform1fv(blurProgram->id(),
                coeffsUniformLocation,
                (GLsizei)gauss_coeffs.size(),
                gauss_coeffs.data());

        // Also store gauss coeffs as ssbo.
        GLuint coeffsSSBOIndex = glGetProgramResourceIndex(blurProgram->id(), GL_SHADER_STORAGE_BLOCK, "GaussCoeffs");
        std::cout << "coeffs ssbo index: " << coeffsSSBOIndex << std::endl;
        // SSBO associated with block binding 0.
        glShaderStorageBlockBinding(blurProgram->id(), coeffsSSBOIndex, 0);
    
        // Create buffer, upload data on creation and bind to binding point 0.
        DataBuffer gaussCoeffsBuffer(GL_SHADER_STORAGE_BUFFER);
        gaussCoeffsBuffer.initStorage(gauss_coeffs.size() * sizeof(float), gauss_coeffs.data(), 0);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, gaussCoeffsBuffer.id());
        
        // Send kernel half size to shader.
        int kernelUniformLocation = glGetProgramResourceLocation(blurProgram->id(), GL_UNIFORM, "kernelHalfSize");
        glProgramUniform1i(blurProgram->id(), kernelUniformLocation, kernelHalfSize);
        
        // Get location of uniform controlling blur direction.
        int blurDirectionUniformLocation = glGetProgramResourceLocation(blurProgram->id(), GL_UNIFORM, "blurDirection");

        std::vector<glm::ivec2> blockConfigs = {
                {16, 16}, {32, 32}, {1,32}, {32, 1},
                {256,1}, {1024,1}
        };
        std::vector<std::vector<float>> times;
        times.resize(blockConfigs.size());

        QueryManager mgr(QueryObject::Target::QOT_TIME_ELAPSED);
    
        blurProgram->use();

        for (int iter = 0; iter < 10; iter++) {
            for (int icfg = 0; icfg < blockConfigs.size(); icfg++) {
                for (int iter2 = 0; iter2 < 10; iter2++) {
                    glm::ivec2 blockSize = blockConfigs[icfg];
                    glm::ivec2 blockCount = (gridSize + blockSize - 1) / blockSize;

                    // Dispatch compute call for variable block size
                    auto query = mgr.beginQuery();
    
                    // First pass: hozirontal blur.
                    glm::ivec2 blurDirection = glm::ivec2(1,0);
                    glProgramUniform2iv(blurProgram->id(), blurDirectionUniformLocation, 1, glm::value_ptr(blurDirection));
                    glBindImageTexture(0, inputTexture->texture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
                    glBindImageTexture(1, intermTexture->texture(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
                    glDispatchComputeGroupSizeARB(blockCount.x, blockCount.y, 1, blockSize.x, blockSize.y, 1);
                    
                    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
                    
                    // Second pass: vertical blur.
                    blurDirection = glm::ivec2(0,1);
                    glProgramUniform2iv(blurProgram->id(), blurDirectionUniformLocation, 1, glm::value_ptr(blurDirection));
                    glBindImageTexture(0, intermTexture->texture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
                    glBindImageTexture(1, outputTexture->texture(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
                    glDispatchComputeGroupSizeARB(blockCount.x, blockCount.y, 1, blockSize.x, blockSize.y, 1);
                    
                    query->endQuery();
                    float result = float((double)query->getResultSync() / 1e6);
                    mgr.processFinishedQueries();
                    if (iter <= 2)
                        continue;
                    times[icfg].push_back(result);
                }
            }
        }

        std::cout.precision(3);

        for (int i = 0; i < blockConfigs.size(); i++) {
            float avgTime = std::accumulate(times[i].cbegin(), times[i].cend(), 0.0f) / times[i].size();
            float sigma = 0.0f;
            for (auto t : times[i])
                sigma += (t - avgTime) * (t - avgTime);
            sigma /= times[i].size();
            sigma = sqrtf(sigma);
            sigma *= 2.0f; // 2-sigma trust interval.
    
            std::cout << blockConfigs[i].x << "x" << blockConfigs[i].y << ":\t\t"
                      << avgTime-sigma << "-"
                      << avgTime+sigma << " ms" << std::endl;
        }

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

//        outputTexture->saveRGBA8_PNG("blurred.png");
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
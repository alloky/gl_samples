#version 430 core

// Usually block size is defined in shader:
layout (local_size_x = 8, local_size_y = 4) in;

// 'restrict' keyword ensures that different variables access different objects: for possible optimizations.
layout (rgba8, binding=0) uniform restrict readonly image2D inputTex;
layout (rgba8, binding=1) uniform restrict writeonly image2D outputTex;

void main() {
    ivec2 coords = ivec2(gl_GlobalInvocationID.xy);
    ivec2 size = imageSize(inputTex);

    // Benefit from using built-in functions.
    if (any(greaterThanEqual(coords, size)))
        return;

    vec4 color = imageLoad(inputTex, coords);
    float gray = dot(color, vec4(0.333333f, 0.333333f, 0.333333f, 0));
    color = vec4(gray, gray, gray, color.a);
    imageStore(outputTex, coords, color);
}

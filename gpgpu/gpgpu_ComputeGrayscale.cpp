#include <Application.hpp>

#include <iostream>
#include <vector>
#include <numeric>

#include <Common.h>
#include <Texture.hpp>
#include <ShaderProgram.hpp>
#include <QueryObject.h>

/**
3 грани куба (вариант без индексов)
*/
class SampleApplication : public Application
{
public:
    //Идентификатор шейдерной программы
    ShaderProgramPtr grayscaleProgram;

    TexturePtr inputTexture;
    TexturePtr outputTexture;

    void run() override {
        // Load shader:
        grayscaleProgram = std::make_shared<ShaderProgram>();
        grayscaleProgram->createProgramCompute("gpgpushaders/grayscale.comp");

        // Load input texture:
        inputTexture = loadTexture("images/image_8k.png");

        int width, height;
        inputTexture->getSize(width, height);

        // Create output texture of the same size:
        outputTexture = std::make_shared<Texture>();
        outputTexture->initStorage2D(1, GL_RGBA8, width, height);

        grayscaleProgram->use();

        // Bind textures to binding points so that shader can access them:
        glBindImageTexture(0, inputTexture->texture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
        glBindImageTexture(1, outputTexture->texture(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);

        glm::ivec2 gridSize(width, height);

        std::vector<glm::ivec2> configs = { {8,4}, {1,1}, {32, 1}, {4, 8},
                                            {16, 16}, {32, 32}, {32,1},
                                            {1,32}, {256,1}, {512, 1}, {1024,1} };
        std::vector<std::vector<float>> times;
        times.resize(configs.size());

        QueryManager mgr(QueryObject::Target::QOT_TIME_ELAPSED);

        for (int iter = 0; iter < 10; iter++) {
            for (int icfg = 0; icfg < configs.size(); icfg++) {
                for (int iter2 = 0; iter2 < 10; iter2++) {
                    glm::ivec2 blockSize = configs[icfg];
                    glm::ivec2 blockCount = (gridSize + blockSize - 1) / blockSize;

                    // Dispatch compute call for variable block size
                    auto query = mgr.beginQuery();
                    glDispatchComputeGroupSizeARB(blockCount.x, blockCount.y, 1, blockSize.x, blockSize.y, 1);
                    query->endQuery();
                    float result = float((double)query->getResultSync() / 1e6);
                    mgr.processFinishedQueries();
                    times[icfg].push_back(result);
                }
            }
        }

        std::cout.precision(3);

        for (int i = 0; i < configs.size(); i++) {
            float avgTime = std::accumulate(times[i].cbegin(), times[i].cend(), 0.0f) / times[i].size();
            float sigma = 0.0f;
            for (auto t : times[i])
            	sigma += (t - avgTime) * (t - avgTime);
            sigma /= times[i].size();
            sigma = sqrtf(sigma);
            sigma *= 2.0f; // 2-sigma trust interval.
            
            std::cout << configs[i].x << "x" << configs[i].y << ":\t\t"
	                << avgTime-sigma << "-"
	                << avgTime+sigma << " ms" << std::endl;
        }

//        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

//        outputTexture->saveRGBA8_PNG("grayscale.png");
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
# Примеры для семинаров по компьютерной графике // ФИВТ МФТИ 2019

Примеры используют библиотеки:

* GLFW (<http://www.glfw.org/>) -- для создания окна и инициализации контекста OpenGL
* GLEW (<http://glew.sourceforge.net/>) -- для загрузки функций API OpenGL
* GLM (<http://glm.g-truc.net>) -- для обеспечения матрично-векторных операций
* Assimp (<http://www.assimp.org/>) -- для импорта 3D моделей
* SOIL (<https://bitbucket.org/SpartanJ/soil2>) -- для чтения изображений
* imgui (<https://github.com/ocornut/imgui>) -- для создания пользовательского интерфейса

Все зависимости входят в репозиторий.
Если возникла необходимость доустановки каких-либо зависимостей, просьба сообщить семинаристу.

## Инструкция по сборке

### Ubuntu

Примерный список необходимых пакетов:
`libgl1-mesa-dev libglu1-mesa-dev libxrandr-dev libxinerama-dev libxcursor-dev libglfw3-dev libglew-dev libglfw3`

Сборка проверена на компиляторе GCC 8.

Ручную сборку можно проводить следующим образом:

```
# Находимся в папке gl_samples
mkdir build
cd build
cmake -DCMAKE_C_COMPILER=/usr/bin/gcc-8 -DCMAKE_CXX_COMPILER=/usr/bin/g++-8 ..
cmake --build . -- -j4
```

### Windows

Visual Studio 2017 позволяет работать с CMake-проектами без конвертации.

#include <Application.hpp>
#include <LightInfo.hpp>
#include <Framebuffer.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <sstream>
#include <assimp/material.h>
#include <thread>


float frand()
{
	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

float frand(float min, float max) {
	return min + (max - min) * frand();
}

GLuint rand(GLuint max) {
	return rand() % max;
}

GLuint rand(GLuint min, GLuint max) {
	return min + rand(max - min);
}

/**
Пример использования bindless текстур.
*/
class SampleApplication : public Application
{
public:
	std::vector<std::thread> jobs;
	int desiredJobsNumber = 12;

	MeshPtr mesh;

	//Идентификатор шейдерной программы
	ShaderProgramPtr _usualShader;
	ShaderProgramPtr _bindlessShader_direct;
	ShaderProgramPtr _bindlessShader_ssbo;

	enum RenderingType {
		Standard,
		BindlessTex_DirectHandleUsage,
		BindlessTex_SSBO_multidraw,
		Indirect_multidraw
	};

	RenderingType renderingType = Standard;

	//Переменные для управления положением одного источника света
	float _lr = 10.0;
	float _phi = 0.0;
	float _theta = 0.48;

	float _lightIntensity = 1.0;
	LightInfo _light;

	// Сколько генерировать разных текстур.
	size_t derivTexturesCount = 100;

	// Для имитации большого количества текстур.
	std::vector<TexturePtr> diffuseTextures;
	std::vector<TexturePtr> specTextures;
	// Для bindless текстур.
	std::vector<GLuint64> diffTextureHandles;
	std::vector<GLuint64> specTextureHandles;
	bool texturesResident = false;
	bool wishResidentTextures = false;

	GLuint _repeatSampler;

	// ----- Scene setup -----
	struct MeshRenderRequest {
		unsigned int first;
		unsigned int count;
		unsigned int diffTexIndex;
		unsigned int specTexIndex;
		glm::vec3 position;
		glm::quat rotation;
	};

	std::vector<MeshRenderRequest> scene;
	GLint objectsCount = 2600;

	GLuint trianglesCountTotal = 0;

	glm::vec3 sceneLBN = glm::vec3(-30, -12, -15);
	glm::vec3 sceneRTF = glm::vec3(-10, 12, 5);

	// ----- Multi-draw rendering -----
	std::vector<GLint> multiDraw_offsets;
	std::vector<GLsizei> multiDraw_counts;

	// ---- SSBOs -----
	struct Material {
		GLuint64 diffuseTex;
		GLuint64 specularTex;
	};

	struct DrawDescription {
		glm::mat4 modelMatrix; // 16 * 4 = 64 bytes
		Material material;
	};

	DataBufferPtr drawcallSSBO;
	bool ssbosOutdated = true;

	// ----- Indirect rendering -----
	struct DrawArraysIndirectCommand {
		GLuint  count;
		GLuint  instanceCount;
		GLuint  first;
		GLuint  baseInstance;

		DrawArraysIndirectCommand() { }
		DrawArraysIndirectCommand(GLuint _count, GLuint _instanceCount, GLuint _first, GLuint _baseInstance) : count(_count), instanceCount(_instanceCount), first(_first), baseInstance(_baseInstance) { }
	};

	DataBufferPtr drawCommands;
	bool drawCommandsOutdated = true;

	// Дополнительная нагрузка на CPU каждый кадр. Количество циклов вызова rand().
	GLint cpuLoadCycles = 1700000;

	volatile bool running = true;

	void initJobs(int njobs) {
		for (int i = 0; i < njobs; i++) {
			jobs.push_back(std::thread([&]() {
				while (running) {
					cpuLoad();
				}
			}));
		}
	}

	void rebuildDrawCommands() {
		drawCommands = std::make_shared<DataBuffer>(GL_DRAW_INDIRECT_BUFFER);

		std::vector<DrawArraysIndirectCommand> commands(scene.size());
		for (size_t i = 0; i < scene.size(); i++) {
			DrawArraysIndirectCommand cmd;
			cmd.count = scene[i].count;
			cmd.instanceCount = 1;
			cmd.first = scene[i].first;
			cmd.baseInstance = 0;
			commands[i] = cmd;
		}

		if (USE_DSA) {
			drawCommands->initStorage(commands.size() * sizeof(DrawArraysIndirectCommand), commands.data(), 0);
		}
		else {
			drawCommands->setData(commands.size() * sizeof(DrawArraysIndirectCommand), commands.data());
		}
	}

	void rebuildSSBOs() {
		assert(texturesResident);
		drawcallSSBO = std::make_shared<DataBuffer>(GL_SHADER_STORAGE_BUFFER);

		std::vector<DrawDescription> drawDescriptions(scene.size());
		for (size_t i = 0; i < scene.size(); i++) {
			const MeshRenderRequest &req = scene[i];
			DrawDescription descr;
			descr.modelMatrix = glm::translate(glm::mat4(1), req.position) * glm::mat4_cast(req.rotation);
			descr.material.diffuseTex = diffTextureHandles[req.diffTexIndex];
			descr.material.specularTex = specTextureHandles[req.specTexIndex];
			drawDescriptions[i] = descr;
		}
		// В общем случае при загрузке в видеопамять нужно беспокоиться о смещениях полей структур.
		// Но в этом примере работает и так :)
		if (USE_DSA) {
			// Zero flags - no limits.
			drawcallSSBO->initStorage(drawDescriptions.size() * sizeof(DrawDescription), drawDescriptions.data(), 0);
		}
		else {
			drawcallSSBO->setData(drawDescriptions.size() * sizeof(DrawDescription), drawDescriptions.data());
		}

		multiDraw_counts = std::vector<GLsizei>(scene.size());
		multiDraw_offsets = std::vector<GLsizei>(scene.size());
		for (size_t i = 0; i < scene.size(); i++) {
			multiDraw_counts[i] = scene[i].count;
			multiDraw_offsets[i] = scene[i].first;
		}
	}

	/**
	 * Генерирует случайную сцену, ставя в сооответствие мэшу со случайными текстурами случайные положения и ориентацию.
	 */
	void generateMeshRenderRequests(std::vector<MeshRenderRequest> &renderRequests, GLuint totalInstances, GLuint totalTextures, GLuint totalMeshes, glm::vec3 minPos, glm::vec3 maxPos) {
		srand(123410231U);
		for (GLuint i = 0; i < totalInstances; i++) {
			MeshRenderRequest req;
			req.count = 3 * rand(std::max(3, mesh->getVertexCount() / 100), mesh->getVertexCount() / 3);
			req.first = rand(0, mesh->getVertexCount() - req.count);
			req.first -= req.first % 3;
			req.diffTexIndex = rand(totalTextures);
			req.specTexIndex = rand(totalTextures);
			req.position = glm::mix(minPos, maxPos, glm::vec3(frand(), frand(), frand()));
			req.rotation = glm::normalize(glm::quat(frand(), frand(), frand(), frand()));
			renderRequests.push_back(req);
		}
	}

	// Вызывает перестроение сцены, пересчитывает статистику и сбрасывает флаги готовности доп. буферов.
	void rebuildScene() {
		scene.clear();
		generateMeshRenderRequests(scene, objectsCount, diffuseTextures.size(), 1, sceneLBN, sceneRTF);

		GLuint totalTriangles = 0;

		for (const auto &req : scene) {
			totalTriangles += req.count / 3;
		}

		trianglesCountTotal = totalTriangles;
		ssbosOutdated = true;
		drawCommandsOutdated = true;
	}

	void initGL() override {
		Application::initGL();

		// Пример не будет работать без поддержки этого расширения.
		if (!GLEW_ARB_bindless_texture) {
			std::cerr << "You need ARB_bindless_texture extension support to run this demo" << std::endl;
			exit(1);
		}
	}

	/**
	 * Делает набор текстур резидентными или нерезидентными.
	 */
	void setHandlesResidency(const std::vector<GLuint64> &handles, bool resident) {
		if (resident) {
			for (GLuint64 handle : handles) {
				glMakeTextureHandleResidentARB(handle);
			}
		}
		else {
			for (GLuint64 handle : handles) {
				glMakeTextureHandleNonResidentARB(handle);
			}
		}
	}

	/**
	 * Генерирует дополнительные текстуры на основе одной исходной и случайных параметров преобразования.
	 */
	void createDerivatedTextures(size_t count, TexturePtr srcTex, std::vector<TexturePtr> &derivatives, std::vector<GLuint64> &derivHandles) {
		GLsizei texWidth, texHeight;
		srcTex->getSize(texWidth, texHeight);

		// Shrink shader.
		ShaderProgram shrinkShader("shaders/quadColor.vert", "gl10shaders/bindless/shrink_colorify.frag");
		shrinkShader.use();

		glActiveTexture(GL_TEXTURE0);
		srcTex->bind();
		shrinkShader.setIntUniform("tex", 0);
		glBindSampler(0, _repeatSampler);

		MeshPtr quad = makeScreenAlignedQuad();

		glViewport(0, 0, texWidth, texHeight);

		glDisable(GL_DEPTH_TEST);
		glEnable(GL_FRAMEBUFFER_SRGB);
		for (size_t i = 0; i < count; i++) {
			Framebuffer renderToTexFBO(texWidth, texHeight);
			TexturePtr deriv = renderToTexFBO.addBuffer(GL_RGB8, GL_COLOR_ATTACHMENT0);
			assert(renderToTexFBO.valid());

			renderToTexFBO.initDrawBuffers();
			renderToTexFBO.bind();

			glm::vec2 shrinkCenter = glm::vec2(frand(), frand());
			float alpha = frand(1, 4);
			glm::vec4 colorFactor = glm::vec4(frand(0.3f, 1.5f), frand(0.3f, 1.5f), frand(0.3f, 1.5f), 1.0f);

			shrinkShader.setVec2Uniform("center", shrinkCenter);
			shrinkShader.setFloatUniform("alpha", alpha);
			shrinkShader.setVec4Uniform("colorFactor", colorFactor);

			// Фокус в том, что к текстурному юниту 0 привязалась другая текстура при создании буфера кадра.
			// Неожиданно и не очень приятно.
			srcTex->bind();

			quad->draw();

			derivatives.push_back(deriv);

			// Deal with texture handles. It does not harm them, so they can still be bound in old-style way.
			GLuint64 texHandle = glGetTextureSamplerHandleARB(deriv->texture(), _repeatSampler);
			derivHandles.push_back(texHandle);
		}
		glDisable(GL_FRAMEBUFFER_SRGB);
		glEnable(GL_DEPTH_TEST);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}

	/**
	 * Имитация работы на CPU.
	 */
	void cpuLoad() {
		for (size_t i = 0; i < cpuLoadCycles; i++) {
			rand();
		}
	}

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//Создание и загрузка мешей

		// Будем использовать один мэш. Но мы же можем вообразить, что просто-напросто загнали несколько в один буфер?
		mesh = makeSphere(0.35f, 25);

		//=========================================================
		//Инициализация шейдеров
		_usualShader = std::make_shared<ShaderProgram>("gl10shaders/bindless/common_simplified.vert", "gl10shaders/bindless/commonSpec.frag");
		_bindlessShader_direct = std::make_shared<ShaderProgram>("gl10shaders/bindless/common_simplified.vert", "gl10shaders/bindless/bindless_direct.frag");
		_bindlessShader_ssbo = std::make_shared<ShaderProgram>("gl10shaders/bindless/drawid_bindless.vert", "gl10shaders/bindless/drawid_bindless.frag");

		//=========================================================
		//Инициализация значений переменных освщения
		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
		_light.ambient = glm::vec3(0.2, 0.2, 0.2);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(1.0, 1.0, 1.0);

		//=========================================================
		//Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
		glGenSamplers(1, &_repeatSampler);
		glSamplerParameteri(_repeatSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_repeatSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_repeatSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

		//=========================================================
		//Загрузка и создание текстур
		auto _brickDiffuseTex_original = loadTexture("images/brick.jpg", SRGB::YES); //sRGB
		auto _brickSpecTex_original = loadTexture("images/brick_spec.jpg", SRGB::NO);
		createDerivatedTextures(derivTexturesCount, _brickDiffuseTex_original, diffuseTextures, diffTextureHandles);
		createDerivatedTextures(derivTexturesCount, _brickSpecTex_original, specTextures, specTextureHandles);

		// Генерация сцены.
		rebuildScene();

		initJobs(desiredJobsNumber);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}

			if (ImGui::DragInt("Objects count", &objectsCount, 50, 1, 100000)) {
				rebuildScene();
			}

			ImGui::LabelText("Scene polygons", "%d", trianglesCountTotal);

			ImGui::Checkbox("Resident textures", &wishResidentTextures);

			ImGui::RadioButton("Bindable samplers", reinterpret_cast<int *>(&renderingType), RenderingType::Standard);
			ImGui::RadioButton("Bindless, direct", reinterpret_cast<int *>(&renderingType), RenderingType::BindlessTex_DirectHandleUsage);
			ImGui::RadioButton("Bindless, ssbo", reinterpret_cast<int *>(&renderingType), RenderingType::BindlessTex_SSBO_multidraw);
			ImGui::RadioButton("Indirect", reinterpret_cast<int *>(&renderingType), RenderingType::Indirect_multidraw);

			ImGui::DragInt("CPU load", &cpuLoadCycles, std::max(1, (int)(0.01f * cpuLoadCycles)), 10, 10000000);
		}
		ImGui::End();
	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);
	}

	void update() override
	{
		Application::update();

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
	}

	/**
	 * Indirect rendering: параметры команд отрисовки читаются из буфера на видеокарте.
	 * Uniform-переменные для каждой модели зашиты в массив.
	 * Элемент массива определяется индексом gl_DrawID.
	 * Также используются bindless текстуры.
	 */
	void drawBindlessSSBO_indirect() {
		if (ssbosOutdated) {
			rebuildSSBOs();
			ssbosOutdated = false;
		}
		if (drawCommandsOutdated) {
			rebuildDrawCommands();
			drawCommandsOutdated = false;
		}

		_bindlessShader_ssbo->use();

		_bindlessShader_ssbo->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_bindlessShader_ssbo->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_bindlessShader_ssbo->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_bindlessShader_ssbo->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
		_bindlessShader_ssbo->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
		_bindlessShader_ssbo->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);

		// Привязываем SSBO с матрицами модели и материалами к точкам привязки.
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, drawcallSSBO->id());

		glBindVertexArray(mesh->getVAO());
		drawCommands->bind();
		// В буфере записано scene.size() команд, выполняем их.
		glBindBuffer(GL_DRAW_INDIRECT_BUFFER, drawCommands->id());
		glMultiDrawArraysIndirect(GL_TRIANGLES, nullptr, scene.size(), 0);
	}

	/**
	 * В целом, как indirect rendering, только параметры multidraw-команды уходят с CPU.
	 */
	void drawBindlessSSBO() {
		if (ssbosOutdated) {
			rebuildSSBOs();
			ssbosOutdated = false;
		}

		_bindlessShader_ssbo->use();

		_bindlessShader_ssbo->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_bindlessShader_ssbo->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_bindlessShader_ssbo->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_bindlessShader_ssbo->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
		_bindlessShader_ssbo->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
		_bindlessShader_ssbo->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);

		// Привязываем SSBO с матрицами модели и материалами к точкам привязки.
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, drawcallSSBO->id());

		mesh->multiDrawArrays(scene.size(), multiDraw_offsets, multiDraw_counts);
	}

	/**
	 * Стандартный подход, использующий texture handles как uniform переменные (не привязывает текстуры к юнитам).
	 * Пользы от этого варианта мало, т.к. активно посылаются uniform переменные.
	 */
	void drawBindlessDirect() {
		_bindlessShader_direct->use();

		_bindlessShader_direct->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_bindlessShader_direct->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_bindlessShader_direct->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_bindlessShader_direct->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
		_bindlessShader_direct->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
		_bindlessShader_direct->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);

		GLint diffuseSamplerLocation = glGetUniformLocation(_bindlessShader_direct->id(), "diffuseTex");
		GLint specularSamplerLocation = glGetUniformLocation(_bindlessShader_direct->id(), "specularTex");

		for (const MeshRenderRequest &req : scene) {
			glm::mat4 modelMatrix = glm::translate(glm::mat4(1), req.position) * glm::mat4_cast(req.rotation);

			glUniformHandleui64ARB(diffuseSamplerLocation, diffTextureHandles[req.diffTexIndex]);
			glUniformHandleui64ARB(specularSamplerLocation, specTextureHandles[req.specTexIndex]);

			_bindlessShader_direct->setMat4Uniform("modelMatrix", modelMatrix);
			mesh->drawArrays(req.first, req.count);
		}
	}

	/**
	 * Стандартный подход.
	 */
	void drawSimple() {
		_usualShader->use();

		_usualShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_usualShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);


		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
		_usualShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
		_usualShader->setVec3Uniform("light.La", _light.ambient * _lightIntensity);
		_usualShader->setVec3Uniform("light.Ld", _light.diffuse * _lightIntensity);
		_usualShader->setVec3Uniform("light.Ls", _light.specular * _lightIntensity);


		glBindSampler(0, _repeatSampler);
		glBindSampler(1, _repeatSampler);
		_usualShader->setIntUniform("diffuseTex", 0);
		_usualShader->setIntUniform("specularTex", 1);

		for (const MeshRenderRequest &req : scene) {
			glm::mat4 modelMatrix = glm::translate(glm::mat4(1), req.position) * glm::mat4_cast(req.rotation);

			std::vector<GLuint> textures = { diffuseTextures[req.diffTexIndex]->texture(), specTextures[req.specTexIndex]->texture() };
			glBindTextures(0, textures.size(), textures.data());
			_usualShader->setMat4Uniform("modelMatrix", modelMatrix);
			mesh->drawArrays(req.first, req.count);
		}

		glBindSampler(0, 0);
		glBindSampler(1, 0);
	}

	void draw() override
	{
		GLsizei width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

		// Deal with residency.
		if (renderingType != Standard)
			wishResidentTextures = true;
		if (wishResidentTextures != texturesResident) {
			setHandlesResidency(diffTextureHandles, wishResidentTextures);
			setHandlesResidency(specTextureHandles, wishResidentTextures);
			texturesResident = wishResidentTextures;
		}

		// Draw according to chosen method.
		switch (renderingType) {
			case Standard:
				drawSimple();
				break;
			case BindlessTex_DirectHandleUsage:
				drawBindlessDirect();
				break;
			case BindlessTex_SSBO_multidraw:
				drawBindlessSSBO();
				break;
			case Indirect_multidraw:
				drawBindlessSSBO_indirect();
				break;
			default:
				assert(false);
				break;
		}
	}

	void onStop() override {
		Application::onStop();
		running = false;
		for (auto &job : jobs) {
			job.join();
		}
	}
};

int main()
{
	SampleApplication app;
	app.start();

	return 0;
}
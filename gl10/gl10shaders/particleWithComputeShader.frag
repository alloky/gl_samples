#version 330

uniform sampler2D tex;
uniform sampler1D palette;

in float velocity;

uniform float exponent;

layout(location = 0) out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec4 color = texture(tex, gl_PointCoord); //читаем из текстуры
	float paletteTexCoord = 1 - exp(-exponent * velocity);
//    float paletteTexCoord = velocity;
	vec3 gradient = texture(palette, paletteTexCoord).rgb;
	color.rgb *= gradient; // домножаем на зависимость от скорости
	
	fragColor = color;
}
